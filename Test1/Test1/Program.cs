﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Test1
{
    public class Program
    {
        public int sum(int value1, int value2)
        {
            return value1 + value2;
        }
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
            
            // Added from GitLab 
            // Newly added from GitLab 2

            // Line added from Visual studio 2

            // Changes of INC#5555 

            // Conflict Code  from VS 
            
            // Conflict code added from GitLab
        }

        
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
